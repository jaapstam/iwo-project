FILE=`mktemp`

#the 2016/01/20160101\:11.out.gz part can be changed to other availabe dates if wished
#to use this script acces is needed to the RUG's /net/corpora directory and the get-gender.py is needed. The get-gender.py script is available in the same repository as this iwo script. To use this script by your own, change the location of the get-gender.py to the location where you have stored this file.

zless /net/corpora/twitter2/Tweets/2016/01/20160101\:11.out.gz | /net/corpora/twitter2/tools/tweet2tab user hashtags | python3 /home/s3240282/dutchnames-master/get-gender.py > $FILE

ALLMALE=`grep -w 'male' $FILE | wc -l`

MALEHASH=`grep -w 'male' $FILE | fgrep -o \# | wc -l`

MALEAVA=`echo "scale=4; $MALEHASH/$ALLMALE" | bc`

ALLFEMALE=`grep -w 'female' $FILE | wc -l`

FEMALEHASH=`grep -w 'female' $FILE | fgrep -o \# | wc -l`

FEMALEAVA=`echo "scale=4; $FEMALEHASH/$ALLFEMALE" | bc`

echo $ALLMALE "tweets posted by a male counted, with a total of" $MALEHASH "hashtags. This gives an avarage of" $MALEAVA "hashtag per tweet."
echo $ALLFEMALE "tweets posted by a female counted, with a total of" $FEMALEHASH "hashtags. This gives an avarage of" $FEMALEAVA "hashtag per tweet."
rm -f $FILE

